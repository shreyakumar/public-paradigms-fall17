;; utility functions
(define firsts
  (lambda (lat)
    (cond
      ((null? lat) '())
      (else (cons (car (car lat))
                  (firsts (cdr lat)))))))

(define member?
  (lambda (a lat)
    (cond
      ((null? lat) #f)
      ((eq? (car lat) a) #t)
      ((member? a (cdr lat)) #t)
      (else #f))))

(define subset?
  (lambda (set1 set2)
    (cond
      ((null? set1) #t)
      ((member? (car set1) set2) (subset? (cdr set1) set2))
      (else #f))))

;; data variables
(define dict  '((y o l o) (r o f l) (s a u c e) (c h i c k e n) (f r i e d) (f l a m e s)))
(define words '((o n l y) (l i v e) (r o l l i n g) (l a u g h i n g) (m a n i f o l d) (f l o o r) (o n c e) (o n t h e) (f u e l g a u g e) (t e m p g a u g e) (y o u) (l a n d i n g g e a r l i g h t)))


;; acronym finder
(define acronyms
;; your code here ;-)

(display (acronyms dict words))
(display "\n")


;; output should be:
;; ((y o l o) (r o f l))

