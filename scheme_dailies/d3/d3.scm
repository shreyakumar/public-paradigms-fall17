;; scheme daily homework 3
;; name: ??????????
;; date: ??????????



;; add1 and sub1 are defined here and provided for you to use

;; function add1
(define add1
  (lambda (n)
    (+ n 1)))

;; function sub1
(define sub1
  (lambda (n)
    (- n 1)))

;; double
(define double 
  (lambda (n)
    n))
    ;; TODO: write the function double so that it works using the primitives
    ;; right now double always returns the same number given to it
    ;; make it return *double* that number
    ;; remember that ``number'' means positive integer, for now
    ;; use *only* the functions add1, sub1, and zero?
    ;; do *not* use +, -, *, /, etc.

;; tests!
(display (double 9))
(display "\n")

(display (double 2))
(display "\n")

(display (double 45))
(display "\n")

;; correct output:
;;   $ guile d3.scm
;;   18
;;   4
;;   90

